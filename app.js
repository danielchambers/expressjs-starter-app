var configuration = require('./configuration');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var morgan = require('morgan');
var passport = require('passport');
var app = express();

// settings
app.disable('x-powered-by');
app.set('port', configuration._port);
app.set('view engine', configuration._viewEngine);

// middleware
app.use(morgan(configuration._morgan.development));
app.use(bodyParser.urlencoded(configuration._bodyParser));
app.use(cookieParser());
app.use(session(configuration._session));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes/main'));
app.use('/admin', require('./routes/admin'));

app.listen(configuration._port, function () {
    console.log('Listening at port %s', configuration._port);
});