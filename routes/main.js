var express = require('express');
var router = express.Router();


router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.get('/', function (req, res) {
    res.send('home page');
});

router.post('/register', function (req, res) {
    res.send('home page');
});

router.post('/login', function (req, res) {
    res.send('home page');
});

router.post('/logout', function (req, res) {
    res.send('home page');
});

module.exports = router;