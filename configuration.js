module.exports = {
    _port: process.env.PORT || 3000,
    _viewEngine: 'ejs',
    _mongodb: {
        developmentURI: 'mongodb://localhost:27017/demoapp',
        productionURI: ''
    },
    _bodyParser: {
        extended: true
    },
    _morgan: {
        development: 'combined'
    },
    _session: {
        resave: true,
        saveUninitialized: true,
        secret: '0oi9uyt5r4edrfghjkoiuytrfd3s4d5f6gyhuj'
    }
};